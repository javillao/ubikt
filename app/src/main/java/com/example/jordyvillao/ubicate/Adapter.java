package com.example.jordyvillao.ubicate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter extends BaseAdapter {

    private Context context;
    private ArrayList<Device> listDevices;

    public Adapter(Context context, ArrayList<Device> listDevices) {
        this.context = context;
        this.listDevices = listDevices;
    }

    @Override
    public int getCount() {
        return listDevices.size();
    }

    @Override
    public Object getItem(int position) {
        return listDevices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Device item = (Device) getItem(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.item, null);
        TextView tvDevice = (TextView) convertView.findViewById(R.id.txtNameDevice);

        tvDevice.setText(item.getNameDevice());

        return convertView;
    }
}
