package com.example.jordyvillao.ubicate;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private EditText txtEmail;
    private EditText txtPass;
    private Button btnLogin;
    private Button btnSign;
    private ImageView imgLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPass = (EditText) findViewById(R.id.txtPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnSign = (Button) findViewById(R.id.btnSign);
        imgLogo = (ImageView) findViewById(R.id.imgLogo);

        imgLogo.setImageResource(R.drawable.ubikt);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(txtEmail.getText().toString(),txtPass.getText().toString());
                System.out.println(txtEmail.getText() +"\t"+txtPass.getText());
            }
        });

        btnSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtEmail.getText().toString().isEmpty() || txtPass.getText().toString().isEmpty())
                    System.out.println("Campos vacios.");
                else
                    System.out.println("Revise su correo y autentique su cuenta");
            }
        });
    }

    private void validate(String email, String password){
        if(email.equals("myuser1") && password.equals("mypass1")){
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            startActivity(intent);
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Not valid username or password");
            builder.setTitle("Error de login");
            builder.setPositiveButton("Ok", null);
            builder.setCancelable(true);
            builder.create().show();
        }
    }

}
