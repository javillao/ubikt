package com.example.jordyvillao.ubicate;

public class Device {

    private String code;
    private String nameDevice;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameDevice() {
        return nameDevice;
    }

    public void setNameDevice(String nameDevice) {
        this.nameDevice = nameDevice;
    }

    public Device(String code, String nameDevice) {
        this.code = code;
        this.nameDevice = nameDevice;

    }

    @Override
    public boolean equals(Object obj) {
        if( obj == this)
            return true;
        if(!(obj instanceof Device))
            return false;
        Device d = (Device)obj;
        return d.getCode()==this.getCode();
    }

    @Override
    public String toString(){
        return this.getCode()+","+this.getNameDevice();
    }
}
