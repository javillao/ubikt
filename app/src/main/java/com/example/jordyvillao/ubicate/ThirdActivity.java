package com.example.jordyvillao.ubicate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {

    private TextView txtName, txtCode;
    private Intent intent;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        txtName = (TextView) findViewById(R.id.txtName);
        txtCode = (TextView) findViewById(R.id.txtCode);

        intent = getIntent();
        if(intent.getIntExtra("requestCode", 1)==SecondActivity.DEVICE_NEW_NAME_REQUEST){
            String name = intent.getStringExtra("Device").split(",")[1];
            String code = intent.getStringExtra("Device").split(",")[0];
            txtCode.setText(name);
            txtCode.setText(code);
            txtCode.setFocusable(false);
        }



    }
    public void addDevice(View view){
        String name = txtName.getText().toString();
        String code = txtCode.getText().toString();
        if(name.isEmpty() || code.isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(view, "Existen campos vacios", Snackbar.LENGTH_LONG);
            snackbar.show();
        }else{
            intent.putExtra("Info", code+","+name);
            setResult(RESULT_OK,intent);
            finish();
        }
    }

    public void saveChanges(View view){
        String name = txtName.getText().toString();
        if(name.isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(view, "Existen campos vacios", Snackbar.LENGTH_LONG);
            snackbar.show();
        }else{
            intent.putExtra("newDeviceName",txtCode.getText().toString()+","+name);
            setResult(RESULT_OK,intent);
            finish();
        }

    }
}
