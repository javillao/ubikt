package com.example.jordyvillao.ubicate;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import java.util.ArrayList;

public class SecondActivity extends AppCompatActivity {

    public final static int DEVICE_INFO_REQUEST = 1;
    public final static int DEVICE_NEW_NAME_REQUEST = 2;

    public static Context context;
    public static Device selectedDevice = null;

    private ArrayList<Device> arrayDevices = new ArrayList<>();
    private ListView listDevices;
    private Adapter adapter;
    private Button btnAdd, btnEdit, btnDelete, btnTrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        context = getApplicationContext();
        listDevices = (ListView) findViewById(R.id.lstViewDevices);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnEdit = (Button) findViewById(R.id.btnEdit);
        btnTrack = (Button) findViewById(R.id.btnTrack);

        adapter = new Adapter(this, arrayDevices);
        listDevices.setAdapter(adapter);

        listDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedDevice = (Device) listDevices.getItemAtPosition(position);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                startActivityForResult(intent, DEVICE_INFO_REQUEST);
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(selectedDevice == null){
                    Snackbar snackbar = Snackbar
                            .make(v, "No hay un dispositivo seleccionado", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else{
                    Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                    intent.putExtra("Device",selectedDevice.toString());
                    startActivityForResult(intent, DEVICE_NEW_NAME_REQUEST );
                }

            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedDevice == null){
                    Snackbar snackbar = Snackbar
                            .make(v, "No hay un dispositivo seleccionado", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else{
                    arrayDevices.remove(selectedDevice);
                    adapter = new Adapter(context, arrayDevices);
                    listDevices.setAdapter(adapter);
                }
            }
        });
        btnTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedDevice == null){
                    Snackbar snackbar = Snackbar
                            .make(v, "No hay un dispositivo seleccionado", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else{
                    Intent i = new Intent(SecondActivity.this, FourthActivity.class);
                    startActivity(i);
                }
            }
        });

    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode){
        intent.putExtra("requestCode",requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==DEVICE_INFO_REQUEST && resultCode==RESULT_OK){
            String[] arrData = data.getStringExtra("Info").split(",");
            Device newItem = new Device(arrData[0], arrData[1]);
            updateListview(newItem);

        }else if(requestCode==DEVICE_NEW_NAME_REQUEST && resultCode==RESULT_OK){
            String[] arrData = data.getStringExtra("newDeviceName").split(",");
            Device newItem = new Device(arrData[0],arrData[1]);
            replaceDevice(newItem);
        }
    }

    public void replaceDevice(Device newDevice){
        for(Device d: arrayDevices){
            if(d.equals(newDevice))
                d.setNameDevice(newDevice.getNameDevice());
        }
        adapter = new Adapter(this, arrayDevices);
        listDevices.setAdapter(adapter);
    }

    public void updateListview(Device device){
        arrayDevices.add(device);
        adapter = new Adapter(this, arrayDevices);
        listDevices.setAdapter(adapter);
    }
}
